﻿using System;
using UnityEngine;

[Serializable]
public class AnimatorLink 
{
    public string identifier;
    public Animator animator;
}
