﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntroController : MonoBehaviour {

    void Start()
    {
        StartCoroutine(Example());
        
    }

    IEnumerator Example()
    {
        Debug.Log("WATING");
        yield return MyCoroutine.WaitForRealSeconds(8);
        SceneFader.instance.FadeOut("Gameplay");
        Debug.Log("DONE");
    }

    public void Skip()
    {

        SceneFader.instance.FadeOut("Gameplay");
    }
}
