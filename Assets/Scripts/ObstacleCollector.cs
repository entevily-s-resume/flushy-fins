﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleCollector : MonoBehaviour {
    private GameObject[] obstacleHolders;
    private float distance = 5f;
    private float lastObstacleX = 0;

    private float obstacleMin = -3.24f;
    private float obstacleMax = .69f;

    private void Awake()
    {
        obstacleHolders = GameObject.FindGameObjectsWithTag("ObstacleHolder");

        for(int i =- 0; i < obstacleHolders.Length; i++)
        {
            Vector3 temp = obstacleHolders[i].transform.position;
            temp.y = Random.Range(obstacleMin, obstacleMax);
            obstacleHolders[i].transform.position = temp;
            if(lastObstacleX < obstacleHolders[i].transform.position.x)
            {
                lastObstacleX = obstacleHolders[i].transform.position.x;
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(GameplayController.instance.GetScore() < (PlayerPrefs.GetInt("SM") * 1050) + 1047)
        {
            if (collision.tag == "ObstacleHolder")
            {
                Vector3 temp = collision.transform.position;
                temp.x = lastObstacleX + distance;
                temp.y = Random.Range(obstacleMin, obstacleMax);
                collision.transform.position = temp;
                lastObstacleX = temp.x;
            }
        }
    }
}
