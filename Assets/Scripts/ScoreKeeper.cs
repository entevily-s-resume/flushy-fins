﻿using UnityEngine;

public class ScoreKeeper : MonoBehaviour {
    #region SINGLETON PATTERN
    public static ScoreKeeper _instance;
    public static ScoreKeeper Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<ScoreKeeper>();

                if (_instance == null)
                {
                    GameObject container = new GameObject("ScoreKeeper");
                    _instance = container.AddComponent<ScoreKeeper>();
                }
            }
            return _instance;
        }
    }
# endregion
    private int score;

    public int GetScore()
    {
        return score;
    }

    public void SetScore(int score)
    {
        this.score = score;
    }
}
