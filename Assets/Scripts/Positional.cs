﻿using System;

[Serializable]
public struct Positional
{
    public float left;
    public float right;
    public Panel panel;
}