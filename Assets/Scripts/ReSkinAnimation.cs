﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class ReSkinAnimation : MonoBehaviour
{
    public string spriteSheetName;
    private List<Vector2> test;

    private PolygonCollider2D pc2d;

    private void Awake()
    {
        pc2d = GetComponent<PolygonCollider2D>();
    }

    private void Update()
    {
        GenerateCollider(GetComponent<SpriteRenderer>().sprite);
    }

    private void LateUpdate()
    {
        var subSprites = Resources.LoadAll<Sprite>("characters/" + spriteSheetName);
        foreach(var renderer in GetComponentsInChildren<SpriteRenderer>())
        {
            string spriteName = renderer.sprite.name;
            var newSprite = Array.Find(subSprites, item => item.name == spriteName);

            if (newSprite)
            {
                renderer.sprite = newSprite;
            }

        }
    }

    private void GenerateCollider(Sprite sprite)
    {
        sprite = GetComponent<SpriteRenderer>().sprite;
        //for (int i = 0; i < pc2d.pathCount; i++)
        //{
        //    if(i=0)
        //    pc2d.SetPath(i, );
        //}
        pc2d.pathCount = sprite.GetPhysicsShapeCount();

        List<Vector2> path = new List<Vector2>();
        for (int i = 0; i < pc2d.pathCount; i++)
        {
            path.Clear();
            sprite.GetPhysicsShape(i, path);
            pc2d.SetPath(i, path.ToArray());
        }
    }
}
