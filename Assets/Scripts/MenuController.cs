﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuController : MonoBehaviour
{
    #region SINGLETON PATTERN
    public static MenuController _instance;
    public static MenuController Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<MenuController>();

                if (_instance == null)
                {
                    GameObject container = new GameObject("MenuController");
                    _instance = container.AddComponent<MenuController>();
                }
            }
            return _instance;
        }
    }
    #endregion
    [SerializeField]
    private GameObject[] fishies;

    [SerializeField]
    private GameObject playGameButton, leaderBoardButton, loader, leaderBoardDisabledButton;

    [SerializeField]
    private GameObject settingsPanel, mainPanel;

    [SerializeField]
    private Slider musicSlider, fxSlider;

    [SerializeField]
    private Button FXMuteButton, MusicMuteButton;

    [SerializeField]
    private Sprite muteButton, muteButtonEnabled;

    [SerializeField]
    private AudioClip[] sounds;

    private bool isTurdFishUnlocked;

    private void Start()
    {
        if(SceneManager.GetActiveScene().name == "main_menu")
        {
            //fishies[GameController.instance.GetSelectedFish()].SetActive(true);
            CheckWhichFishAreUnlocked();
            PlayerPrefs.SetInt("SM", 0);
            Time.timeScale = 1f;
        }
        
    }

    public void SignInOutGooglePlay()
    {
        PlayServicesController.Instance.SignInOrOutOfGooglePlay();
    }

    private void HideLoadingScreen()
    {
        loader.SetActive(false);
    }

    private void CheckWhichFishAreUnlocked()
    {
        if (GameController.Instance.IsTurdFishUnlocked() == 1)
        {
            isTurdFishUnlocked = true;
        }
    }

    public void ShowBuyScreen()
    {
        SceneManager.LoadScene(4);
    }

    public void PlayerLoginStateHasChanged()
    {
        Debug.Log("Should show button on actual game if authenticated.");
        if (Social.localUser.authenticated)
        {
            leaderBoardButton.SetActive(true);
            leaderBoardDisabledButton.SetActive(false);
            HideLoadingScreen();
        }
        else
        {
            leaderBoardButton.SetActive(false);
            leaderBoardDisabledButton.SetActive(true);
            HideLoadingScreen();
        }
    }

    public void PlayGame()
    {
        Debug.Log("Calling fadein from menu controller");
        if (GameController.Instance.GetIntroWatched() == 1)
        {
            SceneFader.Instance.FadeOut("main_game");
        }
        else
        {
            SceneFader.Instance.FadeOut("intro_cut");
        }
        
    }

    public void OpenLeaderboardsScoreUI()
    {
        PlayServicesController.Instance.OpenLeaderboradsScore();
    }

    public void ChangeFish()
    {
        Debug.Log("Try changing fish");
        if (GameController.Instance.GetSelectedFish() == 0)
        {
            Debug.Log("I AM 0");
            if (isTurdFishUnlocked)
            {
                Debug.Log("Turd is unlocked");
                fishies[0].SetActive(false);
                GameController.Instance.SetSelectedFish(1);
                fishies[GameController.Instance.GetSelectedFish()].SetActive(true);
                PlayServicesController.Instance.UnlockAchievement(FlushyFishResources.achievement_a_floater);
                Debug.Log("set as turd");
            }
        }
        else if (GameController.Instance.GetSelectedFish() == 1)
        {
            Debug.Log("I am turd");
            fishies[1].SetActive(false);
            GameController.Instance.SetSelectedFish(0);
            UnlockableManager.Instance.Unlock("poo");
            fishies[GameController.Instance.GetSelectedFish()].SetActive(true);
        }
    }

    public void SetFXVolume(float volume)
    {
        AudioController.Instance.SetFXVolume(volume);
        if (fxSlider.value == -80f)
        {
            FXMuteButton.image.sprite = muteButtonEnabled;
        }
        else
        {
            FXMuteButton.image.sprite = muteButton;
        }

        PlayRandomFX();
    }

    private void PlayRandomFX()
    {
		int sound = UnityEngine.Random.Range (0, 2);
		if (sound == 0)
			GetComponent<AudioSource> ().volume = .5f;
        GetComponent<AudioSource>().PlayOneShot(sounds[sound]);

		GetComponent<AudioSource> ().volume = 1;
    }

    public void SetMusicVolume(float volume)
    {
        AudioController.Instance.SetMusicVolume(volume);
        if (musicSlider.value == -80f)
        {
            MusicMuteButton.image.sprite = muteButtonEnabled;
        }
        else
        {
            MusicMuteButton.image.sprite = muteButton;
        }
    }

    private float previousFXVolume = 0f;
    private float previousMusicVolume = 0f;

    public void MuteFXVolume()
    {
        if (fxSlider.value == -80f)
        {
            fxSlider.value = previousFXVolume;
            FXMuteButton.image.sprite = muteButton;
        }
        else
        {
            previousFXVolume = fxSlider.value;
            fxSlider.value = -80f;
            FXMuteButton.image.sprite = muteButtonEnabled;
        }
    }

    public void MuteMusicVolume()
    {
        //AudioController.instance.SetMusicVolume(-80f);
        if (musicSlider.value == -80f)
        {
            musicSlider.value = previousMusicVolume;
            MusicMuteButton.image.sprite = muteButton;
        }
        else
        {
            previousMusicVolume = musicSlider.value;
            musicSlider.value = -80f;
            MusicMuteButton.image.sprite = muteButtonEnabled;
        }

    }


    public void SwapMenus()
    {
        settingsPanel.SetActive(!settingsPanel.activeSelf);
        mainPanel.SetActive(!settingsPanel.activeSelf);
        if (settingsPanel.activeSelf)
        {
            musicSlider.value = AudioController.Instance.GetMusicVolume();
            fxSlider.value = AudioController.Instance.GetFXVolume();
        }
        else
        {
            GameController.Instance.SetFXVolume(fxSlider.value);
            GameController.Instance.SetMusicVolume(musicSlider.value);
        }

    }

    public void KeepPlaying()
    {
        PlayerPrefs.SetInt("SM", PlayerPrefs.GetInt("SM") + 1);
        SceneFader.Instance.FadeOut("main_game");
    }

    public void ToMainMenu()
    {
        SceneFader.Instance.FadeOut("main_menu");
    }

}
