﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterForce : MonoBehaviour {
    public float force;
	
	// Update is called once per frame
	void Update () {
        GetComponent<Rigidbody2D>().AddForce(Vector2.left * force);
    }
}