﻿using System;
using UnityEngine;

public class GameController : MonoBehaviour {

    public static GameController instance;
    private const string HIGH_SCORE = "High Score";
    private const string SELECTED_FISH = "Selected Fish";
    private const string TURD_FISH = "Turd Fish";
    private const string LOGIN_WAS_CANCELLED = "Login Cancelled";
    private const string FX_VOLUME = "FX Volume";
    private const string MUSIC_VOLUME = "Music Volume";
    private const string VERSION = "Current Version"; // Current bundle version, used to remove, add player pref defaults in version changes.

    private const string WATCHED_INTRO = "Watched Intro";

    private void Awake()
    {
        MakeSingleton();
        CheckVersioningFixes();
        IsTheGameStartedForTheFirstTime();

        //PlayerPrefs.DeleteAll();
        //Debug.Log("Deleting all player prefersences");
    }

    private void CheckVersioningFixes()
    {
        if (!PlayerPrefs.HasKey(VERSION))
        {
            PlayerPrefs.SetInt(TURD_FISH, 1);
            PlayerPrefs.SetFloat(MUSIC_VOLUME, 0);
            PlayerPrefs.SetFloat(FX_VOLUME, 0);
            PlayerPrefs.SetInt(VERSION, 23);
        }
    }

    private void MakeSingleton()
    {
        if(instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    private void IsTheGameStartedForTheFirstTime()
    {
        if (!PlayerPrefs.HasKey("IsTheGameStartedForTheFirstTime"))
        {
            PlayerPrefs.SetInt(HIGH_SCORE, 0);
            PlayerPrefs.SetInt(SELECTED_FISH, 0);
            PlayerPrefs.SetInt(TURD_FISH, 1);
            PlayerPrefs.SetInt("IsTheGameStartedForTheFirstTime", 0);
            PlayerPrefs.SetInt(LOGIN_WAS_CANCELLED, 0);
            PlayerPrefs.SetFloat(MUSIC_VOLUME, 0);
            PlayerPrefs.SetFloat(FX_VOLUME, 0);
        }
        if (!PlayerPrefs.HasKey(WATCHED_INTRO))
        {
            PlayerPrefs.SetInt(WATCHED_INTRO, 0);
        }
    }

    public void IntroWasWatched()
    {
        PlayerPrefs.SetInt(WATCHED_INTRO, 1);
    }

    public int GetIntroWatched()
    {
        return PlayerPrefs.GetInt(WATCHED_INTRO);
    }

    public void SetHighScore(int score)
    {
        PlayerPrefs.SetInt(HIGH_SCORE, score);
    }

    public int GetHighScore()
    {
        return PlayerPrefs.GetInt(HIGH_SCORE);
    }

    public void SetSelectedFish(int selectedTurd)
    {
        PlayerPrefs.SetInt(SELECTED_FISH, selectedTurd);
    }

    public int GetSelectedFish()
    {
        return PlayerPrefs.GetInt(SELECTED_FISH);
    }

    public void UnlockCornTurd()
    {
        PlayerPrefs.SetInt(TURD_FISH, 1);
    }

    public int IsTurdFishUnlocked()
    {
        return PlayerPrefs.GetInt(TURD_FISH);
    }

    public bool WasLoginCancelled()
    {
        if(PlayerPrefs.GetInt(LOGIN_WAS_CANCELLED) == 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    
    public void LoginWasCancelled()
    {
        PlayerPrefs.SetInt(LOGIN_WAS_CANCELLED, 1);
    }

    public void SetMusicVolume(float volume)
    {
        PlayerPrefs.SetFloat(MUSIC_VOLUME, volume);
    }

    public void SetFXVolume(float volume)
    {
        PlayerPrefs.SetFloat(FX_VOLUME, volume);
    }
    
    public float GetMusicVolume()
    {
        return PlayerPrefs.GetFloat(MUSIC_VOLUME);
    }

    public float GetFXVolume()
    {
        return PlayerPrefs.GetFloat(FX_VOLUME);
    }
}
