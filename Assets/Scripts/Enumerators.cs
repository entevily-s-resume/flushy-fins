﻿public enum UnlockableCategory
{
    COMMON,
    UNCOMMON,
    RARE,
    SECRET
}

