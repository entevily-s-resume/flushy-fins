﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TurdController : MonoBehaviour {
    #region SINGLETON PATTERN
    public static TurdController _instance;
    public static TurdController Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<TurdController>();

                if (_instance == null)
                {
                    GameObject container = new GameObject("TurdController");
                    _instance = container.AddComponent<TurdController>();
                }
            }
            return _instance;
        }
    }
    #endregion
    [SerializeField]
    private Rigidbody2D myRigid;

    [SerializeField]
    private Animator anim;

    private float forwardSpeed = 3f;

    private float bounceSpeed = 4f;

    private bool didSwagger;

    public bool isAlive;

    [SerializeField]
    private AudioSource audioSource;

    [SerializeField]
    private AudioClip swagClip, pointClip, dieClip;

    public int score;

    private void Awake()
    {
        if (SceneManager.GetActiveScene().buildIndex != 0 && SceneManager.GetActiveScene().buildIndex != 4)
            anim.SetBool("menu", false);
        else
            anim.SetBool("menu", true);

        if (anim.GetBool("menu"))
            return;

        isAlive = true;

        score = 0;

        score += PlayerPrefs.GetInt("SM") * 1050;

        //GameplayController.instance.SetScore(score);
        
        
        GameplayController.Instance.clickScreenEvent.AddListener(() => FlapThePoo());
        SetCameraX();
    }

    private void Update()
    {
        if (anim.GetBool("menu"))
            return;

        if (isAlive)
        {
            Vector3 temp = transform.position;
            temp.x += forwardSpeed * Time.deltaTime;
            transform.position = temp;
            if (didSwagger)
            {
                didSwagger = false;
                myRigid.velocity = new Vector2(0, bounceSpeed);
                audioSource.PlayOneShot(swagClip);
                anim.SetBool("flap",true);
            }

            if(myRigid.velocity.y >= 0)
            {
                transform.rotation = Quaternion.Euler(0, 0, 0);
            }
            else
            {
                float angle = 0;
                angle = Mathf.Lerp(0, -45, -myRigid.velocity.y /10 );
                transform.rotation = Quaternion.Euler(0, 0, angle);
            }
        }
    }

    public void FlapThePoo()
    {
        if (isAlive)
            didSwagger = true;
    }

    public void FlapPooOver()
    {
        anim.SetBool("flap", false);
    }

    private void SetCameraX()
    {
        CameraFollow.offsetX = (Camera.main.transform.position.x - transform.position.x);
    }

    public float GetPositonX()
    {
        return transform.position.x;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "CF" || collision.gameObject.tag == "Obstacle")
        {
            if (isAlive)
            {
                isAlive = false;
                anim.SetBool("dead", true);
                audioSource.PlayOneShot(dieClip);
                GameplayController.Instance.PlayerDiedShowScore(score);

            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "ObstacleHolder")
        {
            score++;
            GameplayController.Instance.SetScore(score);
			audioSource.volume = .5f;
            audioSource.PlayOneShot(pointClip);
			audioSource.volume = 1f;
        }
        else if(collision.tag == "END")
        {
            SceneFader.Instance.FadeOut("final_cut");
            PlayServicesController.Instance.UnlockAchievement(FlushyFishResources.achievement_there_is_an_end);
            
        }
    }

    
    
}
