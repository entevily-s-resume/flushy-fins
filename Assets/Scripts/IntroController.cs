﻿using System.Collections;
using UnityEngine;

public class IntroController : MonoBehaviour {

    void Start()
    {
        StartCoroutine(Example());
        
    }

    IEnumerator Example()
    {
        Debug.Log("WATING");
        yield return MyCoroutine.WaitForRealSeconds(8);
        SceneFader.Instance.FadeOut("main_game");
        Debug.Log("DONE");
    }

    public void Skip()
    {

        SceneFader.Instance.FadeOut("main_game");
    }
}
