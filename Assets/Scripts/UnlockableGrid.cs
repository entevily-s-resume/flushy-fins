﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnlockableGrid : MonoBehaviour
{
    public UnlockableCategory category;

    [SerializeField]
    private GameObject buttonPrefab;

    private void Start()
    {
        foreach(UnlockableScriptableObject s in UnlockableManager.Instance.unlockables)
        {
            if(s.category == category)
            {
                var g = Instantiate(buttonPrefab, transform);
                g.GetComponent<UnlockButton>().SetAs(s);
                if (UnlockableManager.Instance.IsUnlocked(s.identifier))
                {
                    g.GetComponent<UnlockButton>().Show();
                }
            }
        }
    }
}
