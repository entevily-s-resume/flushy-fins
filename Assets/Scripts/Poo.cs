﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Poo : MonoBehaviour {
    // Movement speed
    public float speed = 2;
    
    // Flap force
    public float force = 300;

    // Animator
    public Animator anim;

    // Use this for initialization
    void Start () {    
        // Fly towards the right
        GetComponent<Rigidbody2D>().velocity = Vector2.right * speed;
    }

    
    
    // Update is called once per frame
    void Update () {
        // Flap
        if (!anim.GetBool("swim"))
        {
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                anim.SetBool("swim", true);
                GetComponent<Rigidbody2D>().AddForce(Vector2.up * force);
            }
        }
    }

    public void AnimComplete()
    {
        anim.SetBool("swim", false);
    }
    
    void OnCollisionEnter2D(Collision2D coll) {
        
        if(coll.collider.tag != "TP"){
            Debug.Log(coll.collider.name + " : : " + coll.collider.tag);
            SceneManager.LoadScene("main_game");
        }
        // Restart
       
    }
}