﻿using System.Collections.Generic;
using UnityEngine;

public class PanelMover : MonoBehaviour
{
    public List<Positional> positions = new List<Positional>();
    public GameObject content;
    private RectTransform contentRect;
    private bool notDragging = false;
    private bool positioning = true;
    Positional closestPanel;
    int curPanel = 0;

    private void Start()
    {
        contentRect = content.GetComponent<RectTransform>();
        closestPanel = positions[curPanel];
    }

    public void Move(int dir)
    {
        curPanel += dir;
        if (curPanel >= positions.Count)
            curPanel = positions.Count;
        if (curPanel <= 0)
            curPanel = 0;

        positioning = true;
    }

    private void Update()
    {
        var rectTransform = GetComponent<RectTransform>();
        var mousePosition = Input.mousePosition;
        var normalizedMousePosition = new Vector2(mousePosition.x / Screen.width, mousePosition.y / Screen.height);
        if (normalizedMousePosition.x > rectTransform.anchorMin.x &&
            normalizedMousePosition.x < rectTransform.anchorMax.x &&
            normalizedMousePosition.y > rectTransform.anchorMin.y &&
            normalizedMousePosition.y < rectTransform.anchorMax.y)
        {
            if (Input.GetMouseButtonDown(0))
            {
                positioning = false;
            }
        }


       

        if (Input.GetMouseButtonUp(0))
        {
            notDragging = true;
        }

        if (notDragging)
        {
            var contentLeft = contentRect.anchoredPosition.x;
            Debug.Log(contentLeft);
            var closestLeft = Mathf.Abs ( closestPanel.left - contentLeft);

            int x = 0;
            foreach (Positional panel in positions)
            {
                if(Mathf.Abs( panel.left - contentLeft) < closestLeft)
                {
                    closestPanel = panel;
                    curPanel = x;
                    closestLeft = Mathf.Abs    ( panel.left - contentLeft);
                }
                x++;
            }
            positioning = true;
            notDragging = false;
            
        }

        if (positioning)
        {
            contentRect.anchoredPosition = new Vector2(Mathf.Lerp(contentRect.anchoredPosition.x, positions[curPanel].left, Time.deltaTime * 10f), contentRect.anchoredPosition.y);
            if(Mathf.Abs(contentRect.anchoredPosition.x - closestPanel.left) <= .1f)
            {
                positioning = false;
            }
        }

        
    }
}
