﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;


public class UnlockButton : MonoBehaviour
{
    [SerializeField]
    private Image bgImage;
    [SerializeField]
    private GameObject questionMark;
    [SerializeField] private GameObject sprite;
    UnlockableScriptableObject unlockableObject;

    private void Start()
    {
        bgImage.color = BrightColor();
        GetComponent<Button>().onClick.AddListener(() => Select());
    }

    private void Select()
    {
        if (UnlockableManager.Instance.IsUnlocked(unlockableObject.identifier))
        {
            UnlockableManager.Instance.Selected(unlockableObject);
        }
        
    }

    private Color BrightColor()
    {
        var r = UnityEngine.Random.Range(0f, 1f);
        switch (UnityEngine.Random.Range(0, 6))
        {
            case 0:
                return new Color(0, 1, r);
            case 1:
                return new Color(1, 0, r);
            case 2:
                return new Color(1, r, 0);
            case 3:
                return new Color(0, r, 1);
            case 4:
                return new Color(r, 0, 1);
            case 5:
                return new Color(r, 1, 0);
            default:
                return new Color(1, 1, 1);
        }
    }

    internal void Show()
    {
        questionMark.SetActive(false);
        sprite.SetActive(true);
    }

    internal void SetAs(UnlockableScriptableObject s)
    {
        unlockableObject = s;
        sprite.GetComponent<Image>().sprite = s.image;
    }
}
