﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneFader : MonoBehaviour {
    public static SceneFader Instance { get; private set; }

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(this.gameObject);
            //Rest of your Awake code

        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    [SerializeField]
    private GameObject fadeCanvas;

    [SerializeField]
    private Animator fadeAnim;

    public void FadeOut(string levelName)
    {
        StartCoroutine(FadeOutAnimation( levelName));
    }

    public void FadeIn()
    {
        StartCoroutine(FadeInAnimation());
    }

    IEnumerator FadeOutAnimation (string levelName)
    {
        //fadeCanvas.SetActive(true);
        fadeAnim.Play("FadeOut");
        yield return StartCoroutine(MyCoroutine.WaitForRealSeconds(.7f));
		Debug.Log("Level name is " + levelName);
		if (levelName == "main_menu") {
			AudioController.Instance.FadeToMenuMusic();
		} else {
			AudioController.Instance.FadeToGameMusic();
		}
        SceneManager.LoadScene(levelName);
        //fadeCanvas.GetComponent<Canvas>().worldCamera = Camera.main;
        FadeIn();
    }

    IEnumerator FadeInAnimation()
    {
        fadeAnim.Play("FadeIn");
        yield return StartCoroutine(MyCoroutine.WaitForRealSeconds(20f));
        //fadeCanvas.SetActive(false);
    }
}
