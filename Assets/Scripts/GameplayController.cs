﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameplayController : MonoBehaviour {
    public static GameplayController instance;

    public int deathcount = 0;

    [SerializeField]
    private Text scoreText, endScore, bestScore, gameOverText;

    [SerializeField]
    private Button restartGame, pauseButton;


    [SerializeField]
    private GameObject pausePanel, instructionsText;

    [SerializeField]
    private GameObject[] fishies;

    [SerializeField]
    private Sprite[] medals;

    [SerializeField]
    private Image medalImage;

    private int currentMultiplier;

    private void Awake()
    {
        MakeInstance();
        fishies[GameController.instance.GetSelectedFish()].SetActive(true);
        currentMultiplier = PlayerPrefs.GetInt("SM");
        Time.timeScale = 0f;
    }

    private void MakeInstance()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    public void PauseGame()
    {
        if(TurdController.instance != null)
        {
            if (TurdController.instance.isAlive)
            {
                pausePanel.SetActive(true);
                pauseButton.gameObject.SetActive(false);
                gameOverText.gameObject.SetActive(false);
                endScore.text = TurdController.instance.score.ToString();
                bestScore.text = GameController.instance.GetHighScore().ToString();
                Time.timeScale = 0f;
                restartGame.onClick.RemoveAllListeners();
                restartGame.onClick.AddListener(() => Resume());
            }
        }
    }

    public void GoToMenu()
    {
        SceneFader.instance.FadeOut("MainMenu");
    }

    public void Resume()
    {
        pausePanel.SetActive(false);
        pauseButton.gameObject.SetActive(true);
        Time.timeScale = 1f;
    }

    public void Restart()
    {
        PlayerPrefs.SetInt("SM", 0);
        SceneFader.instance.FadeOut(SceneManager.GetActiveScene().name);
    }

    public void Play() {
        scoreText.gameObject.SetActive(true);
        pauseButton.gameObject.SetActive(true);
        // Set propper bird up in scene //
        Time.timeScale = 1f;
    }

    private int score;

    public void SetScore(int score)
    {
        this.score = score;
        scoreText.text = score.ToString();
    }

    public int GetScore()
    {
        return score;
    }

    public void PlayerDiedShowScore(int score)
    {
        pausePanel.SetActive(true);
        pauseButton.gameObject.SetActive(false);
        gameOverText.gameObject.SetActive(true);
        scoreText.gameObject.SetActive(false);

        endScore.text = score.ToString();
        if(score > GameController.instance.GetHighScore())
        {
            GameController.instance.SetHighScore(score);
        }

        bestScore.text = GameController.instance.GetHighScore().ToString();

        medalImage.gameObject.SetActive(true);

        if (score >= 1000)
        {
            medalImage.sprite = medals[4];
            PlayServicesController.instance.UnlockAchievement(FlushyFishResources.achievement_king_of_the_drain);
            PlayServicesController.instance.UnlockAchievement(FlushyFishResources.achievement_holy_guacamole);
            PlayServicesController.instance.UnlockAchievement(FlushyFishResources.achievement_just_keep_swimming);
            PlayServicesController.instance.UnlockAchievement(FlushyFishResources.achievement_but_wait_theres_more);
            PlayServicesController.instance.UnlockAchievement(FlushyFishResources.achievement_neighbors_house);
        }
        else if (score >= 250)
        {
            medalImage.sprite = medals[3];
            PlayServicesController.instance.UnlockAchievement(FlushyFishResources.achievement_holy_guacamole);
            PlayServicesController.instance.UnlockAchievement(FlushyFishResources.achievement_just_keep_swimming);
            PlayServicesController.instance.UnlockAchievement(FlushyFishResources.achievement_but_wait_theres_more);
            PlayServicesController.instance.UnlockAchievement(FlushyFishResources.achievement_neighbors_house);
        }
        else if(score >= 100)
        {
            medalImage.sprite = medals[1];
            PlayServicesController.instance.UnlockAchievement(FlushyFishResources.achievement_just_keep_swimming);
            PlayServicesController.instance.UnlockAchievement(FlushyFishResources.achievement_but_wait_theres_more);
            PlayServicesController.instance.UnlockAchievement(FlushyFishResources.achievement_neighbors_house);
        }
        else if(score >= 50)
        {
            
            medalImage.sprite = medals[1];
            PlayServicesController.instance.UnlockAchievement(FlushyFishResources.achievement_but_wait_theres_more);
            PlayServicesController.instance.UnlockAchievement(FlushyFishResources.achievement_neighbors_house);
        }
        else if(score >= 20)
        {
            medalImage.sprite = medals[0];
            PlayServicesController.instance.UnlockAchievement(FlushyFishResources.achievement_neighbors_house);
        }
        
        
        // do stuff in score to unlock turds.

        restartGame.onClick.RemoveAllListeners();
        restartGame.onClick.AddListener(() => Restart());

        AdController.instance.PlayerDied();
    }
}
