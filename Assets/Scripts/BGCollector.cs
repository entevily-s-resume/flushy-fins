﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGCollector : MonoBehaviour {

    private GameObject[] backgrounds;
    private GameObject[] cielsFloors;

    [SerializeField]
    private GameObject end;

    private float lastBGX;
    private float lastCFX;

	// Use this for initialization
	void Start () {
        backgrounds = GameObject.FindGameObjectsWithTag("Background");
        cielsFloors = GameObject.FindGameObjectsWithTag("CF Holder");
        lastBGX = backgrounds[0].transform.position.x;
        lastCFX = cielsFloors[0].transform.position.x;

        for(int i = 1; i < backgrounds.Length; i++)
        {
            if(backgrounds[i].transform.position.x > lastBGX)
            {
                lastBGX = backgrounds[i].transform.position.x;
            }
        }

        for (int i = 1; i < cielsFloors.Length; i++)
        {
            if (cielsFloors[i].transform.position.x > lastCFX)
            {
                lastCFX = cielsFloors[i].transform.position.x;
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {

        if(collision.tag == "Background")
        {
            //Debug.Log("Collide with bg " + collision.name);
            if (GameplayController.instance.GetScore() > (PlayerPrefs.GetInt("SM") * 1050) + 900)
            {

                float color = Mathf.Round(255f - (255f * ((GameplayController.instance.GetScore() - ((PlayerPrefs.GetInt("SM") * 1050) + 900)) / 100f))) / 255f;
                if(color >= 0)
                {
                    Debug.Log(color);
                    Color col = new Color(color, color, color);
                    Debug.Log(col);
                    collision.gameObject.GetComponent<SpriteRenderer>().color = col;
                }
                else
                {
                    collision.gameObject.GetComponent<SpriteRenderer>().color = Color.black;
                }
            }
            
            Vector3 temp = new Vector3(0, 0, 0);
            float width = ((BoxCollider2D)collision).size.x;
            temp.x = lastBGX + width;
            collision.transform.position = temp;
            lastBGX = temp.x;
            if (GameplayController.instance.GetScore() == (PlayerPrefs.GetInt("SM") * 1050) + 1047)
            {
                temp = new Vector3(temp.x + 60, end.transform.position.y, end.transform.position.z);
                end.transform.position = temp;
            }

        }
        else if (collision.tag == "CF Holder")
        {
            Vector3 temp = new Vector3(0, 0, 0);
            float width = ((BoxCollider2D)collision).size.x;
            temp.x = lastCFX + width;
            collision.transform.position = temp;
            lastCFX = temp.x;
        }
    }
}