﻿using UnityEngine;

public class TurdMenu : MonoBehaviour {

    private Vector3 startPos;

	private float randomXOffset = 5f;

	private float minYPos = -3f;
	private float maxYPos = 3f;

	private float minLocalScale = .16f;
	private float maxLocalScale = .7f;

	private float minForwardSpeed = 1f;
	private float maxForwardSpeed = 5f;

	private float rndFwdSpeed = 0f;

    private void Awake()
    {
        startPos = transform.position;
        Vector3 temp = transform.position;
        temp.y = Random.Range(minYPos, maxYPos);
        temp.x = temp.x - Random.Range(0f, randomXOffset);
        transform.position = temp;
		float rndsc = Random.Range (minLocalScale, maxLocalScale);
		Vector3 tempsc = new Vector3 (rndsc, rndsc, rndsc);
		transform.localScale = tempsc;
		rndFwdSpeed = Random.Range (minForwardSpeed, maxForwardSpeed);
    }

    private void FixedUpdate()
    {
        Vector3 temp = transform.position;
        temp.x += rndFwdSpeed * Time.deltaTime;
        transform.position = temp;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Obstacle")
        {
            Vector3 temp = startPos;
            temp.y =  Random.Range(minYPos, maxYPos);
            temp.x = temp.x - Random.Range(0f, randomXOffset);
            transform.position = temp;
			float rndsc = Random.Range (minLocalScale, maxLocalScale);
			Vector3 tempsc = new Vector3 (rndsc, rndsc, rndsc);
			transform.localScale = tempsc;
			rndFwdSpeed = Random.Range (minForwardSpeed, maxForwardSpeed);
        }
    }
}
