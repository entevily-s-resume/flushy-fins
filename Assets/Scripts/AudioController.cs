﻿using System;
using UnityEngine;
using UnityEngine.Audio;

public class AudioController : MonoBehaviour {
    public static AudioController instance;

    [SerializeField]
    private AudioMixer mainMixer;

	[SerializeField]
	private AudioSource menuMusic, gameMusic;

	[SerializeField]
	private float fadeSpeed;

	private bool fadeToGameMusic = false;
	private bool fadeToMenuMusic = false;

	[SerializeField]
	private float musicVolume;

    private void Awake()
    {
        MakeSingleton();
    }

    private void Start()
    {
        SetVolumes();

    }

	private void Update(){
		if (fadeToGameMusic) {
			//Debug.Log ("GAME MUSIC FADING");
			if (menuMusic.volume > 0) {
				menuMusic.volume = menuMusic.volume - fadeSpeed;
			}
			if (gameMusic.volume < musicVolume) {
				gameMusic.volume = gameMusic.volume + fadeSpeed;
			}
			if (gameMusic.volume >= musicVolume && menuMusic.volume == 0) {
				fadeToGameMusic = false;
			}
		}
		if (fadeToMenuMusic) {
			//Debug.Log ("MENU MUSIC FADING");
			if (menuMusic.volume < musicVolume) {
				menuMusic.volume = menuMusic.volume + fadeSpeed;
			}
			if (gameMusic.volume > 0) {
				gameMusic.volume = gameMusic.volume - fadeSpeed;
			}
			if (menuMusic.volume >= musicVolume && gameMusic.volume == 0) {
				fadeToMenuMusic = false;
			}
		}
			
	}

    private void SetVolumes()
    {
        SetMusicVolume(GameController.instance.GetMusicVolume());
        SetFXVolume(GameController.instance.GetFXVolume());
    }

    private void MakeSingleton()
    {
        if(instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    public void SetMusicVolume(float volume)
    {
        mainMixer.SetFloat("MusicVolume", volume);
    }

    public void SetFXVolume(float volume)
    {
        mainMixer.SetFloat("FXVolume", volume);
    }

    public float GetFXVolume()
    {
        float volume;
        mainMixer.GetFloat("FXVolume", out volume);
        return volume;
    }

    public float GetMusicVolume()
    {
        float volume;
        mainMixer.GetFloat("MusicVolume", out volume);
        return volume;
    }

	public void FadeToGameMusic(){
		Debug.Log ("Fade to game music called");
		fadeToGameMusic = true;
		fadeToMenuMusic = false;
	}

	public void FadeToMenuMusic(){
		Debug.Log ("Fade to menu music called");
		fadeToMenuMusic = true;
		fadeToGameMusic = false;
	}
}
