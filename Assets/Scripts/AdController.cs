﻿using UnityEngine;
#if UNITY_ADS
using UnityEngine.Advertisements; // only compile Ads code on supported platforms
#endif

public class AdController : MonoBehaviour {
	public static AdController Instance { get; private set; }

	void Awake()
	{
		if (Instance == null)
		{
			Instance = this;
			DontDestroyOnLoad(this.gameObject);
			//Rest of your Awake code

		}
		else
		{
			Destroy(this.gameObject);
		}
	}
	private int deathcount = 0;
	private bool playShorterAd = false;
	private bool playLongerAd = false;

	private void Start(){
		InvokeAllTimers ();
	}

	public void InvokeAllTimers(){
		InvokeShortTimer ();
		InvokeLongTimer ();
	}

	private void InvokeShortTimer(){
		Invoke ("ShortAdTimePassed", 90);  // Minute and a half
	}

	private void InvokeLongTimer(){
		Invoke ("LongAdTimePassed", 600);  // Ten Minutes
	}

	private void ShortAdTimePassed(){
		playShorterAd = true;
	}

	private void LongAdTimePassed(){
		playLongerAd = true;
	}

	public void PlayerDied()
	{
		deathcount++;

		if (playLongerAd) {
			CancelInvoke (); // Reset the short ad invoke //

			// Reset //
			deathcount = 0;
			playShorterAd = false;
			playLongerAd = false;

			// Show Ad //

			#if UNITY_ADS
			if (!Advertisement.IsReady())
			{
				Debug.Log("Ads not ready for default placement");
				return;
			}
			Advertisement.Show ("longerAd");
			#endif

			// Invoke both ad timers again. //
			InvokeAllTimers ();
		}else if (deathcount >= 5 && playShorterAd == true) {
			// Reset //
			deathcount = 0;
			playShorterAd = false;
			playLongerAd = false;

			#if UNITY_ADS
			if (!Advertisement.IsReady())
			{
				Debug.Log("Ads not ready for default placement");
				return;
			}
			Advertisement.Show();
			#endif

			InvokeShortTimer ();
		}
	}
}
