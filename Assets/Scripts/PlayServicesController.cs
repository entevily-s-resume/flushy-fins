﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using GooglePlayGames;
using System;
using GooglePlayGames.BasicApi;

public class PlayServicesController : MonoBehaviour
{

    public static PlayServicesController instance;

    private void Awake()
    {
        MakeSingleton();
    }

    private void Start()
    {
        PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder().Build();
        PlayGamesPlatform.InitializeInstance(config);
        PlayGamesPlatform.DebugLogEnabled = true;
        PlayGamesPlatform.Activate();
    }

    private IEnumerator WaitAndLogin()
    {
        Debug.Log("Waiting");
        yield return new WaitForSeconds(1);
        LoginToGooglePlayGames();
        Debug.Log("WAITING OVER");
    }

    private void MakeSingleton()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    public void SignInOrOutOfGooglePlay()
    {
        if (Social.localUser.authenticated)
        {
            DisconnectFromGooglePlayGames();
        }
        else
        {
            LoginToGooglePlayGames();
        }
    }

    public void LoginToGooglePlayGames()
    {
        Debug.Log("Trying connect");
        if (Social.localUser.authenticated)
        {
            Debug.Log("Authenticated already so doing nothing.");
            MenuController.instance.PlayerLoginStateHasChanged();
        }
        else
        {
            Debug.Log("Not authenticated, so going to try to authenticate");
            Social.localUser.Authenticate((bool success) => {
                if (success)
                {
                    MenuController.instance.PlayerLoginStateHasChanged();
                }
                else
                {
                    Debug.Log("No success authenticating.");
                    GameController.instance.LoginWasCancelled();
                    MenuController.instance.PlayerLoginStateHasChanged();
                }
            });
        }
    }

    public void DisconnectFromGooglePlayGames()
    {
        if (Social.localUser.authenticated)
        {
            PlayGamesPlatform.Instance.SignOut();
            MenuController.instance.PlayerLoginStateHasChanged();
        }
    }

    public void OpenLeaderboradsScore()
    {
        if (Social.localUser.authenticated)
        {
            PlayGamesPlatform.Instance.ShowLeaderboardUI(FlushyFishResources.leaderboard_high_score);
        }
    }

    void ReportScore(int score)
    {
        Debug.Log("Try Reporting Score");
        if (Social.localUser.authenticated)
        {

            Social.ReportScore(score, FlushyFishResources.leaderboard_high_score, (bool succes) => {
                if (succes)
                {
                    Debug.Log("Score reported");
                }
                else
                {
                    Debug.Log("Scored failed to report.");
                }
            });
        }
        else
        {
            Debug.Log("Score cannot be reported, not authenticated.");
        }
    }


    void OnEnable()
    {
        //Tell our 'OnLevelFinishedLoading' function to start listening for a scene change as soon as this script is enabled.
        SceneManager.sceneLoaded += OnLevelFinishedLoading;
        //Debug.Log("TESTING 123");
    }

    public void UnlockAchievement(string acheivmentCode)
    {
        PlayGamesPlatform.Instance.UnlockAchievement(acheivmentCode);
    }

    void OnDisable()
    {
        //Tell our 'OnLevelFinishedLoading' function to stop listening for a scene change as soon as this script is disabled.
        //Remember to always have an unsubscription for every delegate you subscribe to!
        SceneManager.sceneLoaded -= OnLevelFinishedLoading;
    }

    void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
    {
        Debug.Log("Scene loaded");
        if (Social.localUser.authenticated)
        {
            Debug.Log("Authenticated, pass up my high score.");
            ReportScore(GameController.instance.GetHighScore());
            if (scene.buildIndex == 0)
            {
                Debug.Log("This is main menu and I am already authenticated.");
                MenuController.instance.PlayerLoginStateHasChanged();
            }
        }
        else if (scene.buildIndex == 0 && !GameController.instance.WasLoginCancelled())
        {
            Debug.Log("not authenticated, main menu loaded, login was not cancelled.");
            StartCoroutine(WaitAndLogin());
        }
        else if (scene.buildIndex == 0)
        {
            MenuController.instance.PlayerLoginStateHasChanged();
        }
    }


}
